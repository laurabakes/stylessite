function getRandomPosition(element) {
    var parent = document.getElementById('home');
    
	var x = parent.clientHeight - 60;
    
	var y = parent.clientWidth - 50;

    
	var randomX = Math.floor(Math.random()*x);
	var randomY = Math.floor(Math.random()*y);

    
return [randomX,randomY];
}

function getRandomTimeout(itemIndex) {

    var randomA = Math.floor(Math.random()*10000);
	var randomB = Math.floor(Math.random()*10000);
    var startDelay = parseInt(itemIndex);
    if(randomA < randomB  )
	   return [startDelay + randomA, startDelay + randomB  + 2000];
    else
	   return [startDelay + randomB, startDelay + randomA  + 2000];
        
}


//var myApp = angular.module('stylesApp',['ngAnimate']);
var myApp = angular.module('stylesApp',[]);



function HeaderCtrl($scope) {
    $scope.header = {name: "header.html", url: "header.html"};
}


 angular.module('stylesApp',  ['ngAnimate'])
 
    .directive('snowflake', function($animate, $timeout) {
      return {
          
          restrict: 'AE',
          replace: 'true',
          templateUrl: 'snowflake.html',
           link: function(scope, elem, attrs ) {
               var position = getRandomPosition(elem);
               if(position[0] < 40)
                   position[0] = 40;
               elem.css('top', position[0] + 'px');
               elem.css('left', position[1] + 'px');
               
               scope.hideMe = true;  
               
               var timeDelay = scope.$index * 5000 +( scope.$parent.$index * 21000); 
               
               var timeoutLength = getRandomTimeout(timeDelay );
               $timeout(function () { scope.hideMe = false; }, timeoutLength[0]);
               $timeout(function () { scope.hideMe = true; }, timeoutLength[1]);
               
               elem.bind('click', function() {
                    elem.css('background-color','pink');
                      scope.$apply(function() {
                      scope.color = "white";
                    });
                    
                  });
                  elem.bind('mouseover', function() {
                    elem.css('cursor', 'pointer');
                  });
                }
      };
    })
     .controller('GalleryController', function() {

          var model = this;

          model.faqs = [];
                model.faqs.push({id:'faqOne', question: 'How do I become a Styles Ice Cream stockist?', answer: 'If you are interested in selling our ice cream, please do call or email us at your earliest convenience. We will then arrange to come and see you with samples of our ice cream and further information as to why you should choose Styles!'});
                model.faqs.push({id:'faqTwo', question: 'What sizes can I purchase your ice cream in?', answer: 'Most of our ice cream can be purchased in 120ml, 550ml, 1ltr, 2ltr, 4ltr, 10ltrs or 4.5ltr Napoli’s.'});
                model.faqs.push({id:'faqThree', question: 'What point of sale is available and how much does this cost?', answer: 'Our point of sale material is free and comes as part of the package when you come on board with us, it includes; Pavement signs, flavours boards or menus and flags.'});
                model.faqs.push({id:'faqFour', question: 'Where do you deliver?', answer: 'We have a vast delivery circuit ourselves and we also have external distributors that deliver nationwide.'});
                model.faqs.push({id:'faqFive', question: 'I have a number of food allergies; can I eat your ice cream?', answer: 'All of our ice creams are gluten free and suitable for vegetarians, please click HERE to view our up to date allergens list to ensure our ice cream is right for you.'});
                model.faqs.push({id:'faqSix', question: 'Do you have a minimum order?', answer: 'For customers within the Styles Farmhouse delivery area our minimum order is £50 including VAT, however, if we are in your area than we are more than happy to drop an order in, please call to find out our delivery days for your area.'});
                model.faqs.push({id:'faqSeven', question: 'Why should I choose Styles?', answer: 'Our staff and customers are extremely important to us and we work hard to ensure we have a happy environment. We pride ourselves on ensuring that we provide the best service, making the best luxury farmhouse ice cream, and using the best locally sourced ingredients where possible. Being a family run farm we try hard to make sure that we are there to support our customers when needed and we like to make the process as straight forward as possible. We also offer other products including the full range of cones, tubs, spoons and all popular ice lollies from Walls, Franco’s and Treats to make the ordering process an easy one.  '});
                model.faqs.push({id:'faqEight', question: 'Is my event too big/small for Styles Farmhouse Ice Cream?', answer: 'Here at Styles we firmly believe that no event is too big or too small. Throughout the year we are involved with in excess of 200 shows. These shows range from a village fete or country fair to large scale agricultural shows, steam fairs and music festivals, so please do not hesitate to get in touch if you are questioning whether Styles is right for you!'});


            model.photos = [];

                model.photos.push({src:'images/gallery/9_cones.jpg'});
                model.photos.push({src:'images/gallery/air_show.jpg'});
                model.photos.push({src:'images/gallery/air_show2.jpg'});
                model.photos.push({src:'images/gallery/an_icecream.jpg'});
                model.photos.push({src:'images/gallery/article.jpg'});
                model.photos.push({src:'images/gallery/band.jpg'});
                model.photos.push({src:'images/gallery/Best_farmer.jpg'});
                model.photos.push({src:'images/gallery/Briddicott_harvest.png'});
                model.photos.push({src:'images/gallery/Briddicottfarm.png'});

            model.photosExtra = [];

                model.photosExtra.push({src:'images/gallery/9_cones.jpg'});
                model.photosExtra.push({src:'images/gallery/air_show.jpg'});
                model.photosExtra.push({src:'images/gallery/air_show2.jpg'});
                model.photosExtra.push({src:'images/gallery/an_icecream.jpg'});
                model.photosExtra.push({src:'images/gallery/article.jpg'});
                model.photosExtra.push({src:'images/gallery/band.jpg'});
                model.photosExtra.push({src:'images/gallery/Best_farmer.jpg'});
                model.photosExtra.push({src:'images/gallery/Briddicott_harvest.png'});
                model.photosExtra.push({src:'images/gallery/Briddicottfarm.png'});
                model.photosExtra.push({src:'images/gallery/xmasLambs.jpg'});
                model.photosExtra.push({src:'images/gallery/staff_bbq.jpg'});
                    model.photosExtra.push({src:'images/gallery/sheep_flock.jpg'});
                    model.photosExtra.push({src:'images/gallery/glasto4.jpg'});
                    model.photosExtra.push({src:'images/gallery/freezer_man.jpg'});
                    model.photosExtra.push({src:'images/gallery/sowdens.jpg'});
                    model.photosExtra.push({src:'images/gallery/icecream_therapy.jpg'});

                    model.photosExtra.push({src:'images/gallery/dougie_misty.jpg'});
                    model.photosExtra.push({src:'images/gallery/farmhouse.jpg'});
                    model.photosExtra.push({src:'images/gallery/fleet.jpg'});
                    model.photosExtra.push({src:'images/gallery/glasto.jpg'});

                    model.photosExtra.push({src:'images/gallery/glasto3.jpg'});
                    model.photosExtra.push({src:'images/gallery/in_the_paper.jpg'});
                    model.photosExtra.push({src:'images/gallery/porlock_hill.jpg'});
                    model.photosExtra.push({src:'images/gallery/the_farm.jpg'});
                    model.photosExtra.push({src:'images/gallery/top_of_styles.jpg'});

                    model.photosExtra.push({src:'images/gallery/womad.jpg'});
                    model.photosExtra.push({src:'images/gallery/womad2.jpg'});
                    model.photosExtra.push({src:'images/gallery/womad6.jpg'});

                    model.photosExtra.push({src:'images/gallery/gillingham_shaftsbury_award.jpg'});
                    model.photosExtra.push({src:'images/gallery/boss_man.jpg'});
                    model.photosExtra.push({src:'images/gallery/glasto_the_boss.jpg'});

                    model.photosExtra.push({src:'images/gallery/sowdens2.jpg'});
                    model.photosExtra.push({src:'images/gallery/queue.jpg'});
                    model.photosExtra.push({src:'images/gallery/sowdens3.jpg'});

                    model.photosExtra.push({src:'images/gallery/watchet.jpg'});
                    model.photosExtra.push({src:'images/gallery/bike.jpg'});
                    model.photosExtra.push({src:'images/gallery/henley.jpg'});


            model.team = [];
                model.team.push({src:'images/team/davidB.jpg',  name:'David Baker', job: 'MD - Founder', blurb: ''});
                model.team.push({src:'images/team/sueB.jpg',  name:'Sue Baker', job:'Director', blurb:''});
                model.team.push({src:'images/team/jamesB.jpg', name:'James Baker', job:'Regional Wholesale Manager', blurb:''});
                model.team.push({src:'images/team/JamesMiles.jpg',  name:'James Miles', job: 'Retail Manager', blurb: ''});
                model.team.push({src:'images/team/eileenD.jpg',  name:'Eileen Denley', job:'Office Manager', blurb:''});
                model.team.push({src:'images/team/steveH.jpg', name:'Steve Hunt', job:'Production Manager', blurb:''});
                model.team.push({src:'images/team/markS.jpg',  name:'Mark Smithbone', job: 'Accounts Manager', blurb: ''});
                model.team.push({src:'images/team/lucyN.jpg',  name:'Lucy Needs', job:'Wholesale Representative', blurb:''});
                model.team.push({src:'images/team/rachelA.jpg',  name:'Rachael Abraham', job: 'Production Consultant', blurb: ''});
                model.team.push({src:'images/team/dougel.jpg', name:'Dougal Baker', job:'Security and Meet and Greet!', blurb:''});
                

         model.snowflakes = [];
                model.snowflakes.push({src:'images/xmas/snowflake1.png'});
                model.snowflakes.push({src:'images/xmas/snowflake2.png'});
                model.snowflakes.push({src:'images/xmas/snowflake3.png'});
                model.snowflakes.push({src:'images/xmas/snowflake4.png'});
     
          });
  


jQuery('body').bind('click', function(e) {
    
    if(jQuery(e.target).closest('.navbar').length == 0) {
        // click happened outside of .navbar, so hide
        var opened = $("#navbar").hasClass('collapse in');
      
      if ( opened === true ) {
		$("#navbar").collapse('hide');

	}
    }
});
